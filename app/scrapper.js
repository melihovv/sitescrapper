'use strict';

// TODO: promises
// TODO: es6

var urlUtils = require('url');

var request = require('request');
var cheerio = require('cheerio');
var statusCodeAnalyzer = require('statuscode');
var _ = require('lodash');
var robot = require('robots-parser');

var Page = require('./page');
var log = require('./../lib/log')(module);

/**
 * Site scrapper.
 * @module SiteScraper
 */
module.exports = {
    /**
     * Array of pages.
     * @type {Page[]}
     */
    pages: [],

    /**
     * Base url.
     * @type {String}
     */
    baseUrl: '',

    /**
     * Url hostname.
     * @type {String}
     */
    baseHostname: '',

    /**
     * Scrape site, which is located on the passed url.
     *
     * At first, a request is made with the specified url. If the server respond
     * with the status of 200, the web page is parsed and all valid links are
     * pulled (removed) from it. Then, a request will be made with the next
     * specified url and operations will be repeated. This process repeats
     * until all the links won't be processed.
     *
     * @param {String} url Url.
     * @param {scrapeOver} cb Callback.
     */
    scrape: function (url, cb) {
        log.info('Scrapping site ' + url);

        var parsedUrl = urlUtils.parse(url);
        this.baseUrl = parsedUrl.protocol + '//' + parsedUrl.host;
        this.baseHostname = parsedUrl.hostname;

        this.robotstxtUrl = this.baseUrl + '/robots.txt';
        this.robot = null;

        this.pages.push(new Page({url: url}));
        var _this = this;

        // Get robots.txt.
        log.info('Trying to get robots.txt.');
        request.get(this.robotstxtUrl, function (error, response, body) {
            if (error) {
                return cb(error);
            }

            _this.robot = robot(_this.robotstxtUrl, body);

            scrapeSite(cb);
        });

        function scrapeSite(cb) {
            var page = _this.getPageToScrape();

            // There are no pages to scrape.
            // Scrapping is over.
            if (!page) {
                log.info('Removing invalid pages.');
                _this.removeInvalidPages();

                log.info('Scrapping is over.');
                log.info('Saving pages to db.');

                _this.pages.forEach(function (page) {
                    Page.findOne({url: page.url}, function (error, result) {
                        if (error) {
                            throw error;
                        }

                        // If page doesn't exist save it.
                        if (!result) {
                            log.info('Page with url ' + page.url + ' doesn\'t exit in db.');
                            page.save(function (error) {
                                if (error) {
                                    throw error;
                                }
                            });
                        } else {
                            log.info('Page with url ' + page.url + ' exits in db.');
                        }
                    });
                });

                log.info('I\'m done.');
                return cb(null, _this.pages);
            }

            log.info('Scrapping page with url ' + page.url);

            if (_this.robot.isAllowed(_this.baseUrl + page.path, 'Yandexbot')) {
                log.info('Page with url ' + page.url + ' is allowed by robots.txt.');

                request.get({
                    uri: page.path,
                    baseUrl: _this.baseUrl,
                    followRedirect: false
                }, function (error, response, html) {
                    if (error) {
                        return cb(error);
                    }

                    if (statusCodeAnalyzer.accept(response.statusCode, '2xx')) {
                        log.info('Status code of page with url ' + page.url + ' is 200.');

                        // Construct pages from unique links.
                        var links = _this.getAllLinksFromPage(html);
                        _this.addOnlyUniqueLinks(links);

                        // Fill data about the page.
                        page.fillData(html, response.statusCode);
                    } else {
                        page.fillData('', response.statusCode);
                    }

                    scrapeSite(cb);
                });
            } else {
                log.info('Status code of page with url ' + page.url + ' isn\'t 200.');
                page.fillData('', 0, false);
                scrapeSite(cb);
            }
        }
    },

    /**
     * Get all valid links from page.
     * @param {String} html Page's content.
     * @returns {Array} Array of valid links.
     */
    getAllLinksFromPage: function (html) {
        var $ = cheerio.load(html);
        var links = [];
        var _this = this;

        $('a').each(function (index, element) {
            var href = $(element).attr('href').trim();

            if (Page.checkLink(href, _this.baseHostname) && links.indexOf(href) === -1) {
                links.push(href);
            }
        });

        return links;
    },

    /**
     * Add only unique links to pages.
     * @param {Array} links Links.
     */
    addOnlyUniqueLinks: function (links) {
        var _this = this;

        links.forEach(function (link) {
            var isLinkUnique = !_.find(_this.pages, function (page) {
                return page.path === urlUtils.parse(link).path;
            });

            if (isLinkUnique) {
                _this.pages.push(new Page({url: link}));
            }
        });
    },

    /**
     * Get the next page to be scrapped.
     * @returns {Page} The first page, which haven't been scrapped.
     */
    getPageToScrape: function () {
        return _.find(this.pages, function (page) {
            return !page.isScrapped();
        });
    },

    /**
     * Remove invalid pages.
     * Invalid page is a page which status code is not 200.
     */
    removeInvalidPages: function () {
        _.remove(this.pages, function (page) {
            return !statusCodeAnalyzer.accept(page.statusCode, '2xx');
        });
    }
};

/**
 * Callback, which is called, when site scrape is over.
 * @callback scrapeOver
 * @param {Error} error Error.
 * @param {Page[]} pages Array of scraped pages.
 */
