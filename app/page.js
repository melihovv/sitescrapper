'use strict';

var urlUtils = require('url');

var mongoose = require('./../lib/mongoose');

var schema = new mongoose.Schema({
    url: {
        type: String,
        required: true,
        unique: true
    },
    statusCode: Number,
    indexDate: Date,
    content: String,
    isAllowedByRobotsTxt: Boolean
});

schema.virtual('path').get(function () {
    return this.url && urlUtils.parse(this.url).path || '';
});

/**
 * Fill data about the page, when it has been scrapped.
 * @param {String} content Page's content.
 * @param {number} statusCode Response status code.
 * @param {boolean} [isAllowedByRobotsTxt] If url is allowed by robots.txt.
 */
schema.methods.fillData = function (content, statusCode, isAllowedByRobotsTxt) {
    this.indexDate = new Date();
    this.content = content;
    this.statusCode = statusCode;
    this.isAllowedByRobotsTxt = isAllowedByRobotsTxt || true;
};

/**
 * Return true if page was scrapped, otherwise return false.
 * @returns {boolean}
 */
schema.methods.isScrapped = function () {
    return typeof this.statusCode !== 'undefined';
};

/**
 * Check, if link is valid.
 *
 * Valid link is link, which begins with '/' (no
 * matter, if there are trailing whitespaces before '/'), or link, which
 * hostname equals passed baseHostname.
 *
 * @static
 * @param {String} link Url.
 * @param {String} baseHostname Hostname of the site.
 * @returns {boolean} If link is valid.
 */
schema.statics.checkLink = function (link, baseHostname) {
    var checkingHostname = urlUtils.parse(link).hostname;
    return checkingHostname === baseHostname || link.trim()[0] === '/';
};

module.exports = mongoose.model('Page', schema);

