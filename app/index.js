'use strict';

var config = require('config');
var scrapper = require('./scrapper');

scrapper.scrape(config.get('url'), function (error) {
    if (error) {
        return console.error(error);
    }
});
