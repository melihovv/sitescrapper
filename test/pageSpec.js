var Page = require('./../app/page');

describe('Page', function () {
    'use strict';

    it('must be initialized with passed url', function () {
        var page = new Page({url: 'http://domain.com'});
        page.must.exist();
        page.url.must.equal('http://domain.com');
        page.path.must.equal('/');
    });

    it('must fill data', function () {
        var page = new Page();
        page.fillData('content', 200);
        page.content.must.equal('content');
        page.indexDate.must.be.a.date();
        page.statusCode.must.equal(200);
        page.isAllowedByRobotsTxt.must.equal(true);
    });

    describe('#isScrapped', function () {
        it('must return true', function () {
            var page = new Page({});
            page.fillData('content', 200);
            page.isScrapped().must.be.truthy();
        });

        it('must return false', function () {
            var page = new Page();
            page.isScrapped().must.be.falsy();
        });
    });

    describe('#checkLink', function () {
        it('must return false, if link is empty', function () {
            Page.checkLink('').must.be.falsy();
        });

        it('must return false, if link is started from #', function () {
            Page.checkLink('#popup').must.be.falsy();
            Page.checkLink('   #popup').must.be.falsy();
        });

        it('must return false, if it is emailto link', function () {
            Page.checkLink('mailto:vlad@htmlbook.ru').must.be.falsy();
        });

        it('must return false, if link\'s hostname doesn\'t equal baseDomain', function () {
            Page.checkLink('http://sub.domain.com', 'domain.com').must.be.falsy();
        });

        it('must return true, if link is started from /', function () {
            Page.checkLink('/api/v1/dogs').must.be.truthy();
            Page.checkLink('   /api/v1/dogs').must.be.truthy();
        });

        it('must return true, if link\'s hostname equal baseDomain', function () {
            Page.checkLink('http://domain.com/api/v1/dogs?color=red#popup', 'domain.com').must.be.truthy();
        });
    });
});
