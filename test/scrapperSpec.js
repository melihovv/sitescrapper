var request = require('request');
var sinon = require('sinon');

var scrapper = require('./../app/scrapper');
var Page = require('./../app/page');

describe('Scrapper', function () {
    'use strict';

    beforeEach(function () {
        scrapper.pages = [];
    });

    describe('#getAllLinksFromPage', function () {
        it('must get all valid links', function () {
            var html =
                '<a href="#"></a>' +
                '<a href="/blog"></a>' +
                '<a href=""></a>' +
                '<a href="http://domain.com/api/v1/dogs?color=red"></a>' +
                '<a href="http://sub.example.com/api/"></a>';
            scrapper.baseHostname = 'domain.com';
            scrapper.getAllLinksFromPage(html).must.eql([
                '/blog',
                'http://domain.com/api/v1/dogs?color=red'
            ]);
        });
    });

    describe('#addOnlyUniqueLinks', function () {
        it('must add only unique links', function () {
            var link1 = 'http://domain.com';
            var link2 = 'http://domain.com/dogs';
            scrapper.pages = [
                new Page({url: link1})
            ];
            var expectedLinks = [link1, link2];
            scrapper.addOnlyUniqueLinks(['/', link2]);
            scrapper.pages.forEach(function (page, index) {
                page.url.must.equal(expectedLinks[index]);
            });
        });
    });

    describe('#getPageToScrape', function () {
        it('must return next page to scrape', function () {
            var page1 = new Page({url: 'http://domain.com'});
            var page2 = new Page({url: 'http://domain.com/dogs'});
            scrapper.pages = [
                page1,
                page2
            ];
            page1.fillData('content', 200);
            scrapper.getPageToScrape().must.equal(page2);
        });

        it('must return nothing', function () {
            scrapper.pages = [];
            var demand = require('must');
            demand(scrapper.getPageToScrape()).be.undefined();
        });
    });

    describe('#removeInvalidPages', function () {
        it('must remove pages with status code not equaled to 200', function () {
            var page = new Page({url: 'http://domain.com'});
            page.fillData('content', 300);
            scrapper.pages = [page];
            scrapper.removeInvalidPages();
            scrapper.pages.length.must.equal(0);
        });

        it('must don\' remove pages with status code equaled to 200', function () {
            var page = new Page({url: 'http://domain.com'});
            page.fillData('content', 200);
            scrapper.pages = [page];
            scrapper.removeInvalidPages();
            scrapper.pages.length.must.equal(1);
        });
    });

    describe('#scrape', function () {
        beforeEach(function () {
            this.request = sinon.stub(request, 'get');
        });

        afterEach(function () {
            request.get.restore();
        });

        it('must return one page', function (done) {
            this.request
                .onFirstCall()
                .yields(null, null, '')
                .onSecondCall()
                .yields(null, {statusCode: 200}, '<h1>Hello</h1>');

            scrapper.scrape('http://domain.com', function (error, pages) {
                if (error) {
                    return done(error);
                }

                request.get.calledTwice.must.equal(true);
                pages.length.must.equal(1);
                pages[0].url.must.equal('http://domain.com');
                done();
            });
        });

        it('must return two pages', function (done) {
            this.request
                .onFirstCall()
                .yields(null, null, '')
                .onSecondCall()
                .yields(null, {statusCode: 200}, '<a href="/blog">Hello</a>')
                .onThirdCall()
                .yields(null, {statusCode: 200}, '<h1>Hello</h1>');

            scrapper.scrape('http://domain.com', function (error, pages) {
                if (error) {
                    return done(error);
                }

                request.get.calledThrice.must.equal(true);
                pages.length.must.equal(2);
                pages[0].url.must.equal('http://domain.com');
                pages[1].url.must.equal('/blog');
                done();
            });
        });

        it('must return one page, because statusCode of the second page isn\'t 200', function (done) {
            this.request
                .onFirstCall()
                .yields(null, null, '')
                .onSecondCall()
                .yields(null, {statusCode: 200}, '<a href="/blog">Hello</a>')
                .onThirdCall()
                .yields(null, {statusCode: 301}, '<h1>Hello</h1>');

            scrapper.scrape('http://domain.com', function (error, pages) {
                if (error) {
                    return done(error);
                }

                request.get.calledThrice.must.equal(true);
                pages.length.must.equal(1);
                pages[0].url.must.equal('http://domain.com');
                done();
            });
        });

        it('must return an error', function (done) {
            this.request
                .onFirstCall()
                .yields(null, null, '')
                .onSecondCall()
                .yields(Error('Something went wrong'));

            scrapper.scrape('http://domain.com', function (error) {
                if (error) {
                    error.message.must.be.equal('Something went wrong');
                    done();
                }
            });
        });

        it('must not return pages, which are disallowed by robots.txt', function (done) {
            this.request
                .onFirstCall()
                .yields(null, null, 'User-agent: *\nDisallow: /')
                .onSecondCall()
                .yields(null, {statusCode: 200}, '<a href="/blog">Hello</a>')
                .onThirdCall()
                .yields(null, {statusCode: 200}, '<h1>Hello</h1>');

            scrapper.scrape('http://domain.com', function (error, pages) {
                if (error) {
                    return done(error);
                }

                request.get.calledOnce.must.equal(true);
                pages.length.must.equal(0);
                done();
            });
        });

        it('must not return pages, which are disallowed by robots.txt', function (done) {
            this.request
                .onFirstCall()
                .yields(null, null, 'User-agent: *\nDisallow: /\nUser-agent: Yandexbot\nDisallow: /blog')
                .onSecondCall()
                .yields(null, {statusCode: 200}, '<a href="/blog">Hello</a>')
                .onThirdCall()
                .yields(null, {statusCode: 200}, '<h1>Hello</h1>');

            scrapper.scrape('http://domain.com', function (error, pages) {
                if (error) {
                    return done(error);
                }

                request.get.calledTwice.must.equal(true);
                pages.length.must.equal(1);
                pages[0].url.must.equal('http://domain.com');
                done();
            });
        });

        it('must return an error', function (done) {
            this.request
                .onFirstCall()
                .yields(Error('Something went wrong'), null, '');

            scrapper.scrape('http://domain.com', function (error) {
                if (error) {
                    error.message.must.be.equal('Something went wrong');
                    done();
                }
            });
        });
    });
});
