var nconf = require('nconf');
var path = require('path');
var env = process.env;

if (env.NODE_TYPE === 'app') {
    nconf.argv({
        url: {
            alias: 'u',
            describe: 'Site URL to scrape',
            usage: 'Usage: $0 -u [URL]',
            demand: true
        }
    });
}

nconf
    .env()
    .file({file: path.join(__dirname, './config.json')});

module.exports = nconf;
