var mongoose = require('mongoose');
var config = require('config');

mongoose.connect(config.get('mongoose:uri'));

var gracefulExit = function () {
    mongoose.connection.close(function () {
        console.log('Mongoose default connection with DB : is disconnected through app termination');
        process.exit(0);
    });
};

// If the Node process ends, close the Mongoose connection.
process.on('SIGINT', gracefulExit).on('SIGTERM', gracefulExit);

module.exports = mongoose;
