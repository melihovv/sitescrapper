#!/bin/bash

BIN=./node_modules/.bin
APP=./app
DOCS=./docs
POSTFIX=.cmd
NODE_PATH=NODE_PATH=.
MOCHA_BIN=node_modules/mocha/bin/_mocha
REPORTER=spec
JSCS_CONFIG=.jscs.js
COBERTURA_FILE=coverage/cobertura-coverage.xml
COVERAGE_REPORT=coverage/lcov-report/index.html
BROWSER=chrome
FAIL_UNDER=100
JSHINTRC=.jshintrc
METRICS=metrics
TITLE="Site Scrapper"
COMPLEXRC=.complexrc
ISTANBUL_EXCLUDE=-x config/*.js -x app/mongoose.js

test: lint cover-cobertura diff-cover

lint:
	$(BIN)/jshint$(POSTFIX) . && \
	$(BIN)/jscs$(POSTFIX) -c $(JSCS_CONFIG) .

jsdoc:
	jsdoc$(POSTFIX) -d $(DOCS) -r $(APP)

unit-test:
	export $(NODE_PATH) && \
	$(BIN)/mocha$(POSTFIX)

cover:
	export $(NODE_PATH) && \
	istanbul$(POSTFIX) cover $(ISTANBUL_EXCLUDE) $(MOCHA_BIN) -- -R $(REPORTER) && \
	$(BROWSER) $(COVERAGE_REPORT)

view-uncovered:
	$(BROWSER) $(COVERAGE_REPORT)

cover-cobertura:
	export $(NODE_PATH) && \
	istanbul$(POSTFIX) cover --report cobertura $(MOCHA_BIN) -- -R $(REPORTER)

diff-cover:
	diff-cover $(COBERTURA_FILE) --fail-under=$(FAIL_UNDER)

complexity:
	plato$(POSTFIX) -r -d $(METRICS) -l $(JSHINTRC) -t $(TITLE) $(APP) && \
	cr$(POSTFIX) -c $(COMPLEXRC) $(APP)
